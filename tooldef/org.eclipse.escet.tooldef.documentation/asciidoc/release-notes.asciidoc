/////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2021 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available under the terms
// of the MIT License which is available at https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
/////////////////////////////////////////////////////////////////////////////////

include::_root_attributes.asciidoc[]

indexterm:[release, notes]

[[release-notes-chapter-index]]
== ToolDef release notes

The release notes for the versions of ToolDef and the associated tools, as part of the Eclipse ESCET project, are listed below in reverse chronological order.

The release notes may refer to issues, the details for which can be found at the Eclipse ESCET link:https://gitlab.eclipse.org/eclipse/escet/escet/-/issues[GitLab issues page].

See also the Eclipse ESCET link:https://eclipse.org/escet/release-notes.html[toolkit release notes] covering those aspects that are common to the various Eclipse ESCET tools.


=== Version 0.4

TBD

Improvements and fixes:

* Introduced a brand new website (issue #35).
* Many website URLs have changed due to various website structure changes (issues #35 and #73).
* Various documentation/website textual improvements, style improvements and other changes (issues #35 and #54).


=== Version 0.3

Improvements and fixes:

* The website and Eclipse help now use multi-page HTML rather than a single HTML file, although the website still contains a link to the single-page HTML that allows easily searching the full documentation (issue #36).
* Enabled section anchors for documentation on the website, and disabled section anchors for Eclipse help (issue #36).
* Several small documentation fixes and improvements (issue #166).


=== Version 0.2

This release contains no specific changes for ToolDef.


=== Version 0.1

The first release of ToolDef as part of the Eclipse ESCET project.
This release is based on the initial contribution by the Eindhoven University of Technology (TU/e).

Most notable changes compared to the last TU/e release:

* We no longer use separate language and tool versions.
The `.tooldef2` file extension has been changed to `.tooldef` as part of this change.
