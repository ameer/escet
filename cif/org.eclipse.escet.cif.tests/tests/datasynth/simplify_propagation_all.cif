//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2021 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

// Uncontrollable event propagation.

controllable e5a;
uncontrollable e5b;

plant p5:
  disc int[0..5] x in any;

  location loc1:
    initial;
    marked;
    edge e5a when x >= 3 goto loc2;

  location loc2:
    marked;
    edge e5b goto loc3;

  location loc3;
end

// Marking propagation.

controllable e6a, e6b;

plant p6:
  location loc1:
    initial;
    marked;
    edge e6a goto loc2;
    edge e6b goto loc2;

  location loc2:
    edge e6a goto loc3;

  location loc3:
    edge e6b goto loc4;

  location loc4:
    edge e6a goto loc1;
    edge e6b goto loc5;

  location loc5:
    edge e6a goto loc6;

  location loc6:
    edge e6b goto loc7;

  location loc7;
end
