//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2021 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

controllable e2a, e2b, e2c, e2d;

plant p2:
  disc int[0..5] x in any;

  location:
    initial;
    marked;

    edge e2a do x := x + 1;
    edge e2b when x = 3 or x = 5;
    edge e2c when x >= 2;
    edge e2d when x >= 3;
end

plant e2b needs p2.x >= 3;   // No restriction.
plant p2.x = 1 disables e2c; // No restriction.
plant e2d needs p2.x >= 4;   // Disallow p2.x = 3;
