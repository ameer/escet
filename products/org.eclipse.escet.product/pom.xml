<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2010, 2021 Contributors to the Eclipse Foundation
 
  See the NOTICE file(s) distributed with this work for additional
  information regarding copyright ownership.
 
  This program and the accompanying materials are made available under the terms
  of the MIT License which is available at https://opensource.org/licenses/MIT
 
  SPDX-License-Identifier: MIT
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.eclipse.escet</groupId>
        <artifactId>org.eclipse.escet.root</artifactId>
        <version>0.4.0-SNAPSHOT</version>
        <relativePath>../../</relativePath>
    </parent>

    <artifactId>org.eclipse.escet.product</artifactId>
    <packaging>eclipse-repository</packaging>

    <build>
        <plugins>
            <!-- Customize update site. -->
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>tycho-p2-repository-plugin</artifactId>
                <version>${tycho.version}</version>

                <configuration>
                    <!-- Custom update site name. -->
                    <repositoryName>Eclipse ESCET Update Site (Incubation)</repositoryName>

                    <!-- Custom archive file name (excluding file extension). -->
                    <finalName>eclipse-escet-incubation-${escet.version.enduser}-updatesite</finalName>
                </configuration>
            </plugin>

            <!-- Customize products. -->
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>tycho-p2-director-plugin</artifactId>
                <version>${tycho.version}</version>

                <configuration>
                    <!-- Set archive format to use, per platform. -->
                    <formats>
                        <linux>tar.gz</linux>
                        <macosx>tar.gz</macosx>
                        <win32>zip</win32>
                    </formats>

                    <!-- Set archive file name to use. -->
                    <products>
                        <product>
                            <!-- Product UID to which this configuration applies. -->
                            <id>org.eclipse.escet.product</id>

                            <!-- Custom archive file name prefix. -->
                            <!-- Undocumented, see https://dev.eclipse.org/mhonarc/lists/tycho-user/msg05560.html -->
                            <!-- But available, see https://bugs.eclipse.org/bugs/show_bug.cgi?id=357503 -->
                            <!-- The 'os', 'ws' and 'arch' values are automatically added, as is the file extension. -->
                            <archiveFileName>eclipse-escet-incubation-${escet.version.enduser}</archiveFileName>

                            <!-- Add root folder, to prevent issues when extracting to current folder. -->
                            <rootFolders>
                                <linux>eclipse-escet-${escet.version.enduser}</linux>
                                <macosx>EclipseESCET-${escet.version.enduser}.app</macosx>
                                <win32>eclipse-escet-${escet.version.enduser}</win32>
                            </rootFolders>
                        </product>
                    </products>

                </configuration>

                <!-- Enable additional building of products. -->
                <!-- The update site and product are combined in a single bundle. -->
                <!-- The update site has the binary parts of the product. -->
                <!-- Combining them ensures that the product can be updated. -->
                <!-- However, the Maven packaging type is 'eclipse-repository'. -->
                <!-- So, by default the build only creates an update site. -->
                <!-- The configuration below ensures the build also creates products. -->
                <executions>
                    <execution>
                        <id>materialize-products</id>
                        <goals>
                            <goal>materialize-products</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>archive-products</id>
                        <goals>
                            <goal>archive-products</goal>
                        </goals>
                        <!-- Archive after signing. -->
                        <!-- By default, all are in 'package' phase, leading to signing after archiving. -->
                        <!-- Declare archiving to occur in later phase. -->
                        <phase>pre-integration-test</phase> <!-- Changed from default 'package' phase. -->
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

    <profiles>
        <!-- Additional packaging. -->
        <profile>
            <id>jenkins</id>
            <build>
                <plugins>
                    <!-- Create macOS .dmg package. -->
                    <plugin>
                        <groupId>org.eclipse.cbi.maven.plugins</groupId>
                        <artifactId>eclipse-dmg-packager</artifactId>
                        <version>${eclipse.cbi.version}</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>package-dmg</goal>
                                </goals>
                                <phase>integration-test</phase>
                                <configuration>
                                    <source>${project.build.directory}/products/eclipse-escet-incubation-${escet.version.enduser}-macosx.cocoa.x86_64.tar.gz</source>
                                    <sign>true</sign>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <!-- Signing and notarization. -->
        <profile>
            <id>sign</id>
            <build>
                <plugins>
                    <!-- Sign Windows executables. -->
                    <plugin>
                        <groupId>org.eclipse.cbi.maven.plugins</groupId>
                        <artifactId>eclipse-winsigner-plugin</artifactId>
                        <version>${eclipse.cbi.version}</version>
                        <executions>
                            <execution>
                                <id>sign</id>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                                <phase>package</phase>
                                <configuration>
                                    <signFiles>
                                        <signFile>${project.build.directory}/products/${project.name}/win32/win32/x86_64/eclipse-escet-${escet.version.enduser}/eclipse.exe</signFile>
                                        <signFile>${project.build.directory}/products/${project.name}/win32/win32/x86_64/eclipse-escet-${escet.version.enduser}/eclipsec.exe</signFile>
                                    </signFiles>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                    <!-- Sign macOS apps. -->
                    <plugin>
                        <groupId>org.eclipse.cbi.maven.plugins</groupId>
                        <artifactId>eclipse-macsigner-plugin</artifactId>
                        <version>${eclipse.cbi.version}</version>
                        <executions>
                            <execution>
                                <id>sign</id>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                                <phase>package</phase>
                                <configuration>
                                    <signFiles>
                                        <signFile>${project.build.directory}/products/${project.name}/macosx/cocoa/x86_64/EclipseESCET-${escet.version.enduser}.app</signFile>
                                    </signFiles>
                                    <timeoutMillis>300000</timeoutMillis>
                                    <continueOnFail>${macSigner.forceContinue}</continueOnFail>
                                    <entitlements>${project.basedir}/application.entitlement</entitlements>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                    <!-- Notarize macOS .dmg package. -->
                    <!-- Temporarily disabled notarization. It fails as JustJ 11.0.2 is not correctly signed. -->
                    <!--
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <version>3.0.0</version>
                        <executions>
                            <execution>
                                <phase>post-integration-test</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>bash</executable>
                                    <commandlineArgs>macos-notarize-dmg.sh</commandlineArgs>
                                    <useMavenLogger>true</useMavenLogger>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    -->

                </plugins>
            </build>
        </profile>

    </profiles>

</project>
